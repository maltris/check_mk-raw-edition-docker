#!/bin/bash

OLD="$CRE_VERSION"
NEW="$1"
SITENAME="$SITE_NAME"
SITECOUNT=$(ls -1 /omd/sites/ | wc -l)

function NumericVersion(){
        RETURN=${1//[!0-9]/}
        echo "$RETURN"
}

if [ "$SITECOUNT" -gt 1 ] && [ -z "$SITENAME" ]; then
        echo "More than one site, do not know which to use, aborting."
        exit 1
elif [ "$SITECOUNT" -eq 1 ] && [ -z "$SITENAME" ]; then
        SITENAME=$(ls -1 /omd/sites/)
fi

if [ "$(NumericVersion "$NEW")" -le "$(NumericVersion "$OLD")" ]; then
        echo "$SITENAME: Old version $OLD, new version $NEW, no update required..."
        exit 0
else
        echo "$SITENAME: Old version $OLD, new version $NEW, update required..."

        curl -sLO https://mathias-kettner.de/support/"$NEW"/check-mk-raw-"$NEW"_0.stretch_amd64.deb
        dpkg -i check-mk-raw-"$NEW"_0.stretch_amd64.deb

        omd stop "$SITENAME"
        omd update --conflict install "$SITENAME"
fi

